class GroupModel:
    def __init__(self, id, idProject, maxMember, title, position, listUserInside):
        self.id = id
        self.idProject = idProject
        self.maxMember = maxMember
        self.title = title
        self.position = position
        self.listUserInside = listUserInside
        self.intListUserInside = len(listUserInside)