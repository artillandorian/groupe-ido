class ActivityModel:
    def __init__(self, _key, title, description, isPro, coordinates, address, price, beginningDate, duration, bookable, nbPlaces, handler, participants, imagesPaths, tags):
        self._key = _key
        self.title = title
        self.description = description
        self.isPro = isPro
        self.coordinates = coordinates
        self.address = address
        self.price = price
        self.beginningDate = beginningDate
        self.duration = duration
        self.bookable = bookable
        self.nbPlaces = nbPlaces
        self.handler = handler
        self.participants = participants
        self.imagesPaths = imagesPaths
        self.tags = tags