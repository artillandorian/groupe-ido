import java.net.URL;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class FireFoxTest {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        HashMap<String, Object> vars = new HashMap<String, Object>();
        String Hub = "http://localhost:4444";
        DesiredCapabilities caps = new DesiredCapabilities();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        caps.setBrowserName("firefox");
        driver = new RemoteWebDriver(new URL(Hub), caps);
    }
    @BeforeClass
    public static void setupWebdriverChromeDriver() {

        System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/src/grid/geckodriver.exe");
    }

    /*@After
    public void tearDown() {
        driver.quit();
    }*/
    @Test
    public void testProjectFireFox() throws Exception {
        driver.get("http://localhost/");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Login'])[1]/following::div[1]")).click();
        driver.findElement(By.id("pseudo")).click();
        driver.findElement(By.id("pseudo")).clear();
        driver.findElement(By.id("pseudo")).sendKeys("admin");
        driver.findElement(By.id("pseudo")).sendKeys(Keys.ENTER);
        driver.findElement(By.linkText("Voir la liste des utilisateurs")).click();
        driver.findElement(By.linkText("Groupez-moi!")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Test3'])[2]/following::button[1]")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Test3 - Groupe : 1'])[2]/following::button[1]")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='kjbkjg'])[1]/following::button[1]")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='sbd'])[1]/following::button[1]")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='lisa'])[1]/following::button[1]")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='kjbkjg'])[1]/following::button[1]")).click();
        driver.findElement(By.linkText("Voir la liste des utilisateurs")).click();
        driver.findElement(By.linkText("Groupez-moi!")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Définir un nouveau projet'])[1]/preceding::*[name()='svg'][1]")).click();
        driver.findElement(By.id("pseudo")).click();
        driver.findElement(By.id("pseudo")).clear();
        driver.findElement(By.id("pseudo")).sendKeys("lise");
        driver.findElement(By.id("pseudo")).sendKeys(Keys.ENTER);
    }


    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
